<!doctype html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html class="ie"> <![endif]-->
<html lang="pt">
  <head>
  <title>ANGOLA FASHION WEEK 2014</title>
  <meta name="author" content="Brandscape">
  <meta name="description" content="Angola Fashion Week - O maior evento de moda em Angola">
  <meta name="keywords" content="angola, fashion, fashionweek, luanda">
  <meta property="og:title" content="Angola Fashion Week - O maior evento de moda em Angola"/>
  <meta property="og:url" content="http://www.angolafashionweek2014.com/"/>
  <meta property="og:image" content="http://angolafashionweek2014.com/images/afw_fb.jpg"/>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <link href="/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
  <!--[if IE]>
      <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <![endif]-->
  <base href="/">
  </head>
  <body>

    <slide id="{{slide}}" class="hidden-xs"></slide>

    <section id="nav-mobile" class="visible-xs">

      <nav class="navbar navbar-mobile" role="navigation">

        <div class="container-fluid">

          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/"></a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li ng-class="{ active: isActive('/home')}"><a  href="/sobre">SOBRE</a></li>
              <li ng-class="{ active: isActive('/programa')}"><a  href="/programa">PROGRAMA</a></li>
              <!-- <li ng-class="{ active: isActive('/designers')}"><a href="/criadores">CRIADORES</a></li> -->
              <li ng-class="{ active: isActive('/modelos')}"><a href="/modelos">MODELOS</a></li>
              <li ng-class="{ active: isActive('/sponsors')}"><a href="/sponsors">SPONSORS</a></li><br>
              <li ng-class="{ active: isActive('/contactos')}"><a href="/contactos">CONTACTOS</a></li>
            </ul>
          </div>

        </div>

      </nav>

    </section>

    <section id="nav-container" class="hidden-xs">

        <a class="brand" href="/"><img class="img-responsive" src="/images/afw.png"></a>

        <nav class="menu" role="navigation"></nav>

          <div class="logos-menu">
              <!-- <a href="https://www.facebook.com/angolafashionweek14"><i class="fa fa-instagram"></i></a>
              <a href=s"https://www.facebook.com/angolafashionweek14"><i class="fa fa-twitter-square"></i></a> -->
              <a href="https://www.facebook.com/angolafashionweek14"><i class="fa fa-facebook-square"></i></a>
          </div>

      </div>

    </section>

    <section id="main-page">

        <div ui-view="content"></div>

    </section>


<script src="lib/requirejs/require.js" data-main="js/main.js"></script>
</body>
</html>

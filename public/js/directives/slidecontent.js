define(['./module'], function (directives) {

  directives.directive('slide', function() {

    return {
      restrict: "E",
      controller: function ($scope, $timeout) {

        var content = [
          // {state: 'slideshow-two', time: 4000},
          {state: 'slideshow-three', time: 4000},
          {state: 'slideshow-eight', time: 4000},
          {state: 'slideshow-nine', time: 4000}
        ];

        var index = 0;
        var promise;

        var slide = function() {
            $scope.slide = content[index].state;
            promise = $timeout(slide, content[index].time);
            index = (index < content.length - 1) ? index + 1 : 0;
        };

        slide();

       }
    };

  });
});

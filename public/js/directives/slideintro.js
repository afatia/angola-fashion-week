define(['./module'], function (directives) {

  directives.directive('slideintro', function() {

    return {
      restrict: "C",
      controller: function ($scope, $timeout) {

        var home = [
            {state: 'slideshow-two', time: 4000},
            {state: 'slideshow-four', time: 4000},
            {state: 'slideshow-five', time: 4000},
            {state: 'slideshow-six', time: 4000}
         ];

        var index = 0;
        var promise;

        var slide = function() {
            $scope.slide = home[index].state;
            promise = $timeout(slide, home[index].time);
            index = (index < home.length - 1) ? index + 1 : 0;
        };

        slide();

       }
    };

  });
});

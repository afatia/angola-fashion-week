define(['./module'], function (directives) {

  directives.directive('slide', function() {

    return {
      restrict: "E",
      replace: "true",
      template: '<div id="{{slide}}" class="{{status}}"></div>',
      link: function ($scope, elem, attrs) {
        $scope.status = 'slidecontent';
      }
    };
  });
});

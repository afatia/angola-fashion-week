define(['./module'], function (directives) {

  directives.directive('menu', function() {
    return {
      restrict: "C",
      templateUrl: 'templates/nav.html',
      controller: function ($scope, $location) {
          $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
          };
      }
    };

  });
});

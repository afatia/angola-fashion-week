define([
    'require',
    'angular',
    'app',
    'routes'
], function (require, ng, fastclick) {
    'use strict';

    require(['domReady!'], function (document) {
        ng.bootstrap(document, ['app']);
    });
});

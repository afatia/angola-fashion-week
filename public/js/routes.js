define(['./app'], function (app) {
    'use strict';

    return app.config(function ($locationProvider, $stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('home', {
          url: "/home",
          views: {
              "content": {
                templateUrl: 'views/home.html'
              }
            }
          })
          .state('video', {
            url: "/",
            views: {
                "content": {
                  templateUrl: 'views/video.html',
                  controller: 'videoController'
                }
              }
            })
        .state('sobre', {
          url: "/sobre",
          views: {
              "content": {
                templateUrl: 'views/sobre.html'
              }
            }
          })

        .state('programa', {
          url: "/programa",
          views: {
              "content": {
                templateUrl: 'views/programa.html'
              }
          }
          })
            .state('26jun', {
              url: "/programa/26jun",
              views: {
              "content": {
                templateUrl: 'views/programa/26jun.html'
              }
              }
            })
            .state('27jun', {
              url: "/programa/27jun",
              views: {
                "content": {
                  templateUrl: 'views/programa/27jun.html'
                }
              }
            })
            .state('28jun', {
              url: "/programa/28jun",
              views: {
                  "content": {
                    templateUrl: 'views/programa/28jun.html'
                  }
              }
            })

        .state('criadores', {
          url: "/criadores",
          views: {
              "content": {
                templateUrl: 'views/criadores.html'
              }
          }
          })

        .state('modelos', {
          url: "/modelos",
          views: {
              "content": {
                templateUrl: 'views/modelos.html',
                controller: 'modelosController'
              }
            }
          })

        .state('sponsors', {
          url: "/sponsors",
          views: {
              "content": {
                templateUrl: 'views/sponsors.html',
                controller: 'sponsorsController'
              }
            }
          })

          .state('mediapartners', {
            url: "/mediapartners",
            views: {
                "content": {
                  templateUrl: 'views/mediapartners.html',
                  controller: 'mediapartnersController'
                }
              }
            })

        .state('contactos', {
          url: "/contactos",
          views: {
              "content": {
                templateUrl: 'views/contactos.html'
              }
            }
          });

        $urlRouterProvider.otherwise('/');

        $locationProvider.html5Mode(true);

      }).config(function($httpProvider){

          var interceptor = function($rootScope, $location, $q, Flash){

          var success = function(response){
              return response;
          };

          var error = function(response){
              if (response.status == 401){
                  delete sessionStorage.authenticated;
                  $location.path('/admin');
                  Flash.show(response.data.flash);

              }
              return $q.reject(response);

          };

              return function(promise){
                  return promise.then(success, error);
              };
          };
          $httpProvider.responseInterceptors.push(interceptor);
      }).run(function($http,CSRF_TOKEN){
          $http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;
      });
    });

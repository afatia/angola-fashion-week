define(['./module'], function (controllers) {

  controllers.controller('modelosController', function($scope) {
    $scope.showText = false;

    $scope.modelos = [
    {"nome":"Maria Borges","foto":"mariaborges"},
    {"nome":"Adriane Galisteu","foto":"adrianegalisteu"},
    {"nome":"Sharam Diniz","foto":"sharamdiniz"},
    {"nome":"Paolla Oliveira","foto":"paollaoliveira"},
    {"nome":"Patricia de Jesus","foto":"patriciadejesus"},
    {"nome":"Debora Montenegro","foto":"deboramontenegro"}    
    ];

  });

});

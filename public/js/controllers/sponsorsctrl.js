define(['./module'], function (controllers) {

  controllers.controller('sponsorsController', function($scope) {
    $scope.showText = false;

    $scope.sponsors = [
    {"nome":"Afrodente","foto":"afrodente"},
    {"nome":"Emirais Hotel Spa","foto":"emirais"},
    {"nome":"Cuca","foto":"cuca"},
    {"nome":"Aquafish","foto":"aquafish"},
    {"nome":"Global Seguros","foto":"global"},
    {"nome":"BK","foto":"bk"}
    ];

  });

});

define([
    'angular',
    'angular-route',
    'angular-sanitize',
    'angular-resource',
    'angular-animate',
    'lodash',
    'modernizr',
    'fastclick',
    'ga',
    'bootstrap-js',
    'tweenmax',
    'angular-ui-router',
    // 'facebook',
    './controllers/index',
    './directives/index',
    // './animations/index',
    // './filters/index',
    './services/index'
  ], function (angular) {
      'use strict';

        return angular.module('app', [
            'app.controllers',
            'app.directives',
            // 'app.animations',
            // 'app.filters',
            'app.services',
            'ui.router',
            'ngRoute',
            'ngSanitize',
            'ngAnimate',
            'ngResource'
        ]).constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
  });

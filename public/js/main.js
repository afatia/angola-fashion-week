require.config({

    paths: {
        'domReady': '../lib/requirejs-domready/domReady',
        'angular': '../lib/angular/angular',
        'angular-route': '../lib/angular-route/angular-route',
        'angular-resource': '../lib/angular-resource/angular-resource',
        'angular-sanitize': '../lib/angular-sanitize/angular-sanitize',
        'angular-animate': '../lib/angular-animate/angular-animate',
        'angular-ui-router': '../lib/angular-ui-router/release/angular-ui-router',
        'lodash': '../lib/lodash/dist/lodash',
        'jquery': '../lib/jquery/dist/jquery',
        'tweenmax': '../lib/greensock/src/uncompressed/TweenMax',
        'bootstrap-js': '../lib/bootstrap/dist/js/bootstrap',
        'fastclick': '../lib/fastclick/lib/fastclick',
        'modernizr': '../js/modernizr',
        'ga': '../js/ga'
    },

    shim: {
        'angular': {
          exports: 'angular'
        },
        'angular-route': {
          deps: ['angular']
        },
        'angular-resource': {
          deps: ['angular']
        },
        'angular-sanitize': {
          deps: ['angular']
        },
        'angular-animate': {
          deps: ['angular']
        },
        'angular-ui-router': {
          deps: ['angular']
        },
        'bootstrap-js': {
          deps: ['jquery']
        },
        'tweenmax': {
          deps: ['jquery']
        }
    },

    deps: ['./bootstrap']
});
